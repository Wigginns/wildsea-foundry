# The Wildsea

A FoundryVTT implementation of character sheets for [The Wildsea](https://thewildsea.co.uk/) by Felix Isaacs.

![Player Sheet](https://i.imgur.com/8oHSKRu.png)

## Installation

Copy the following URL into the `Manifest URL` field of the `Install System` dialog and click `Install`

- `https://gitlab.com/api/v4/projects/pacosgrove1%2Fwildsea-foundry/packages/generic/wildsea-foundry/latest/system.json`
